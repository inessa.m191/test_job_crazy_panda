#include "Weapon.h"
#include <iostream>

Weapon::Weapon(EItemType _Type) : Item(_Type)
{
	BaseDamage = 0;
	BuffDamage = 0;
	BaseSpeed = 0;
	BuffSpeed = 0;

	std::string _buf;

	std::cout << "\nEnter the damage value of the new item and press Enter:\n";
	bool _bIsCorrect = false;
	while (!_bIsCorrect)
	{
		std::getline(std::cin, _buf);
		if (_buf[0] > 47 && _buf[0] < 58)
		{
			BaseDamage = std::stod(_buf);
			_bIsCorrect = true;
		}
		else
		{
			std::cout << "Invalid value entered.\n";
		}
	}

	std::cout << "\nEnter the speed value of the new item and press Enter:\n";
	_bIsCorrect = false;
	while (!_bIsCorrect)
	{
		std::getline(std::cin, _buf);
		if (_buf[0] > 47 && _buf[0] < 58)
		{
			BaseSpeed = std::stod(_buf);
			_bIsCorrect = true;
		}
		else
		{
			std::cout << "Invalid value entered.\n";
		}
	}
}

void Weapon::DisplayItem()
{
	Item::DisplayItem();

	std::cout << "\tDamage: " << BaseDamage + BuffDamage;
	if (BuffDamage > 0)
	{
		std::cout << "( " << BaseDamage << " + " << BuffDamage << " )";
	}
	std::cout << std::endl;

	std::cout << "\tSpeed: " << BaseSpeed + BuffSpeed;
	if (BuffSpeed > 0)
	{
		std::cout << "( " << BaseSpeed << " + " << BuffSpeed << " )";
	}
	std::cout << std::endl;
}

void Weapon::SelfBuff(EBuffType _BuffType, double _Value)
{
	if (_BuffType == EBuffType::DAMAGE_BUFF)
	{
		BuffDamage += _Value;
	}

	if (_BuffType == EBuffType::SPEED_BUFF)
	{
		BuffSpeed += _Value;
	}
}
