#include <iostream>
#include <string>
#include <vector>
#include "Types.h"
#include "Inventory.h"
#include "Buff.h"
#include "Item.h"


void DisplayBuffType(EBuffType BuffType)
{
	switch (BuffType)
	{
	case EBuffType::DAMAGE_BUFF: std::cout << "DamageBuff";
		break;
	case EBuffType::PROTECTION_BUFF: std::cout << "ProtectionBuff";
		break;
	case EBuffType::SPEED_BUFF: std::cout << "SpeedBuff";
		break;
	default:
		break;
	}
}

int main()
{
	Inventory newIn;
	std::vector<Buff*> AllBuffs;
	std::string buf;

	bool bExitProgram = false;
	while (!bExitProgram)
	{
		newIn.DisplayInventory();
		std::cout << "Select a number and press Enter:\n";
		std::cout << "\t1 - Add item to inventory.\n";
		std::cout << "\t2 - Add new buff.\n";
		std::cout << "\t3 - Buff inventory item.\n";
		std::cout << "\t4 - Exit program.\n";
		std::getline(std::cin, buf);
		if (buf[0] == 49)
		{
			newIn.AddItemToInventory();
		}
		else if (buf[0] == 50)
		{
			AllBuffs.push_back(new Buff);
		}
		else if (buf[0] == 51)
		{
			int ItemNum = -1;
			Item* BuffItem = nullptr;
			std::vector<Buff*> SuitBuffs;

			std::cout << "Select the item number from the inventory you want to buff and press Enter:\n";
			bool _bIsCorrect = false;
			while (!_bIsCorrect)
			{
				std::getline(std::cin, buf);
				if (buf[0] > 47 && buf[0] < 58)
				{
					ItemNum = std::stoi(buf);
					if (ItemNum > -1 && ItemNum < newIn.GetNOfItems())
					{
						BuffItem = newIn.GetItemByN(ItemNum);
						_bIsCorrect = true;
					}
				}

				if (!_bIsCorrect)
				{
					std::cout << "Invalid number entered.\n";
					std::cout << "Select a number and press Enter:\n";
					std::cout << "\t1 - try again.\n";
					std::cout << "\tAny key - exit to main menu.\n";
					std::getline(std::cin, buf);
					if (buf[0] != 49)
					{
						_bIsCorrect = true;
					}
				}
			}


			if (BuffItem)
			{
				for (auto buff : AllBuffs)
				{
					if (buff->GetBuffType() == EBuffType::PROTECTION_BUFF)
					{
						if (BuffItem->GetType() == EItemType::ARMOUR)
						{
							if (buff->CheckBuffIsSuitForItem(BuffItem))
							{
								SuitBuffs.push_back(buff);
							}
						}
					}
					else
					{
						if (BuffItem->GetType() == EItemType::MELEE || BuffItem->GetType() == EItemType::RANGE)
						{
							if (buff->CheckBuffIsSuitForItem(BuffItem))
							{
								SuitBuffs.push_back(buff);
							}
						}
					}
				}


				std::cout << "\nSelect one of the appropriate buffs for this item and press Enter:\n";
				int SuitBuffsSize = SuitBuffs.size();
				if (SuitBuffsSize > 0)
				{
					for (int i = 0; i < SuitBuffsSize; ++i)
					{
						std::cout << i << ". " << *(SuitBuffs[i]->GetName()) << ": ";
						DisplayBuffType(SuitBuffs[i]->GetBuffType());
						std::cout << " + " << SuitBuffs[i]->GetValue() << std::endl;
					}

					int BuffNum = -1;
					_bIsCorrect = false;
					while (!_bIsCorrect)
					{
						std::getline(std::cin, buf);
						if (buf[0] > 47 && buf[0] < 58)
						{
							BuffNum = std::stoi(buf);
							if (BuffNum > -1 && BuffNum < SuitBuffsSize)
							{
								BuffItem->SelfBuff(SuitBuffs[BuffNum]->GetBuffType(), SuitBuffs[BuffNum]->GetValue());
								_bIsCorrect = true;
							}
						}

						if(!_bIsCorrect)
						{
							std::cout << "Invalid number entered.\n";
							std::cout << "Select a number and press Enter:\n";
							std::cout << "\t1 - try again.\n";
							std::cout << "\tAny key - exit to main menu.\n";
							std::getline(std::cin, buf);
							if (buf[0] != 49)
							{
								_bIsCorrect = true;
							}
						}
					}
				}
				else
				{
					std::cout << "There are no suitable buffs. Press Enter to exit to the main menu.\n";
					std::getline(std::cin, buf);
				}
			}

		}
		else if (buf[0] == 52)
		{
			bExitProgram = true;
		}

		system("cls");

	}

	for (auto buff : AllBuffs)
	{
		delete buff;
	}
}