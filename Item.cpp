#include "Item.h"
#include <iostream>

Item::Item(EItemType _Type) : Type(_Type), Level(0)
{
	std::string _buf;
	std::cout << "\nEnter the name of the new item and press Enter:\n";
	std::getline(std::cin, Name);



	std::cout << "\nEnter the level of the new item and press Enter:\n";
	bool _bIsCorrect = false;
	while (!_bIsCorrect)
	{
		std::getline(std::cin, _buf);
		if (_buf[0] > 47 && _buf[0] < 58)
		{
			Level = std::stoi(_buf);
			_bIsCorrect = true;
		}
		else
		{
			std::cout << "Invalid value entered.\n";
		}
	}


	std::cout << "\nSelect the rarity number of the new item and press Enter:\n";
	std::cout << "\t0 - Common.\n";
	std::cout << "\t1 - Rare.\n";
	std::cout << "\t2 - Epic.\n";
	_bIsCorrect = false;
	while (!_bIsCorrect)
	{
		std::getline(std::cin, _buf);
		if (_buf[0] >= 48 && _buf[0] < 48 + N_OF_ITEM_RARITY)
		{
			Rarity = static_cast<EItemRarity>(std::stoi(_buf.assign(_buf, 0, 1)));
			_bIsCorrect = true;
		}
		else
		{
			std::cout << "Invalid number entered.\n";
		}
	}
	
}

void Item::DisplayItem()
{
	std::cout << Name << std::endl;

	switch (Type)
	{
	case EItemType::MELEE:
		std::cout << "\tMelee\n";
		break;
	case EItemType::RANGE:
		std::cout << "\tRange\n";
		break;
	case EItemType::ARMOUR:
		std::cout << "\tArmour\n";
		break;
	default:
		break;
	}

	std::cout << "\t" << Level << " level\n";

	switch (Rarity)
	{
	case EItemRarity::COMMON:
		std::cout << "\tCommon\n";
		break;
	case EItemRarity::RARE:
		std::cout << "\tRare\n";
		break;
	case EItemRarity::EPIC:
		std::cout << "\tEpic\n";
		break;
	default:
		break;
	}

}
