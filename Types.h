#pragma once
#include <iostream>
#include <string>
constexpr auto N_OF_ITEM_TYPES = 3;
constexpr auto N_OF_ITEM_RARITY = 3;
constexpr auto N_OF_BUFF_TYPES = 3;

enum class EItemType
{
	MELEE,
	RANGE,
	ARMOUR
};

enum class EItemRarity
{
	COMMON,
	RARE,
	EPIC
};

enum class EBuffType
{
	DAMAGE_BUFF,
	PROTECTION_BUFF,
	SPEED_BUFF
};