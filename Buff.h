#pragma once
#include <vector>
#include "Types.h"

class Item;

class Buff
{
	std::string Name;
	std::vector<int> Filters;
	EBuffType Type;
	double Value;

public:
	Buff();

	EBuffType GetBuffType()
	{
		return Type;
	}

	double GetValue()
	{
		return Value;
	}

	std::string* GetName()
	{
		return &Name;
	}

	void AddFilters();

	bool CheckBuffIsSuitForItem(Item* _TestItem);
	bool CheckOneFilter(int _Filter, int _TestProperty, int _Operator = 2);

};

