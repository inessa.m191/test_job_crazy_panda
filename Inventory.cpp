#include "Inventory.h"
#include <iostream>
#include <string>
#include "Item.h"
#include "Weapon.h"
#include "Armour.h"

void Inventory::AddItemToInventory()
{
	std::string _buf;

	bool _bItemAdded = false;
	while (!_bItemAdded)
	{
		std::cout << "Select the type number of the new item and press Enter:\n";
		std::cout << "\t0 - Melee.\n";
		std::cout << "\t1 - Range.\n";
		std::cout << "\t2 - Armour.\n";
		std::getline(std::cin, _buf);
		if (_buf[0] >= 48 && _buf[0] < 48 + N_OF_ITEM_TYPES)
		{
			EItemType _ItemType = static_cast<EItemType>(std::stoi(_buf.assign(_buf, 0, 1)));
			if (_ItemType == EItemType::ARMOUR)
			{
				AllItems.push_back(new Armour(_ItemType));
				_bItemAdded = true;
			}
			else if (_ItemType == EItemType::MELEE || _ItemType == EItemType::RANGE)
			{
				AllItems.push_back(new Weapon(_ItemType));
				_bItemAdded = true;
			}
		}
		else
		{
			std::cout << "Invalid number entered.\n";
			std::cout << "Select a number and press Enter:\n";
			std::cout << "\t1 - try again.\n";
			std::cout << "\tAny key - exit to main menu.\n";
			std::getline(std::cin, _buf);
			if (_buf[0] != 49)
			{
				_bItemAdded = true;
			}
		}
	}

	++NOfItems;

}

void Inventory::DisplayInventory()
{
	int _size = AllItems.size();
	for (int i = 0; i < _size; ++i)
	{
		std::cout << i << ". ";
		AllItems[i]->DisplayItem();
		std::cout << "_______________________________\n";
	}

}
