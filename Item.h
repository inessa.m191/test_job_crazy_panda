#pragma once
#include <string>
#include "Types.h"

class Item
{
	std::string Name;
	EItemType Type;
	int Level;
	EItemRarity Rarity;

public:
	Item(EItemType _Type);

	virtual void DisplayItem();

	virtual void SelfBuff(EBuffType _BuffType, double _Value) = 0;

	EItemType GetType()
	{
		return Type;
	}

	int GetLevel()
	{
		return Level;
	}

	EItemRarity GetRarity()
	{
		return Rarity;
	}

};

