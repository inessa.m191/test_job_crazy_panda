#include "Armour.h"
#include <iostream>

Armour::Armour(EItemType _Type) : Item(_Type)
{
	BaseProtection = 0;
	BuffProtection = 0;

	std::string _buf;

	std::cout << "\nEnter the protection value of the new item and press Enter:\n";
	bool _bIsCorrect = false;
	while (!_bIsCorrect)
	{
		std::getline(std::cin, _buf);
		if (_buf[0] > 47 && _buf[0] < 58)
		{
			BaseProtection = std::stod(_buf);
			_bIsCorrect = true;
		}
		else
		{
			std::cout << "Invalid value entered.\n";
		}
	}

}

void Armour::DisplayItem()
{
	Item::DisplayItem();

	std::cout << "\tProtection: " << BaseProtection + BuffProtection;
	if (BuffProtection > 0)
	{
		std::cout << "( " << BaseProtection << " + " << BuffProtection << " )";
	}
	std::cout << std::endl;


}

void Armour::SelfBuff(EBuffType _BuffType, double _Value)
{
	if (_BuffType == EBuffType::PROTECTION_BUFF)
	{
		BuffProtection += _Value;
	}
}
