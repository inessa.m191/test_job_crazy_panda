#pragma once
#include "Item.h"


class Armour : public Item
{
    double BaseProtection;
    double BuffProtection;

public:
    Armour(EItemType _Type);

    void DisplayItem() override;

    void SelfBuff(EBuffType _BuffType, double _Value) override;
};

