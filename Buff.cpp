#include "Buff.h"
#include <iostream>
#include <string>
#include "Item.h"
#include "Armour.h"
#include "Weapon.h"

bool Buff::CheckBuffIsSuitForItem(Item* _TestItem)
{
	int _CheckType = 0;  //0 - �������� �� �����, 1 - �������� �� �������, 2 - �������� �� ���������
	int _CheckNOfThisType = 0;
	bool _bIsTestSucces = false;

	for (int i = 0; i < Filters.size(); ++i)
	{
		if (Filters[i] == -1)
		{
			if (!_bIsTestSucces && _CheckNOfThisType > 0 && _CheckType!=1)
			{
				return false;
			}

			++_CheckType;
			_CheckNOfThisType = 0;
			_bIsTestSucces = false;

		}
		else
		{
			if (!_bIsTestSucces)
			{
				if (_CheckType == 0)
				{
					_bIsTestSucces = CheckOneFilter(Filters[i], static_cast<int>(_TestItem->GetType()));
				}
				else if (_CheckType == 1)
				{
					_bIsTestSucces = CheckOneFilter(Filters[i+1], _TestItem->GetLevel(), Filters[i]);
					++i;
				}
				else if (_CheckType == 2)
				{
					_bIsTestSucces = CheckOneFilter(Filters[i], static_cast<int>(_TestItem->GetRarity()));
				}

				++_CheckNOfThisType;
			}

			if (_CheckType == 1)
			{
				if (_bIsTestSucces)
				{
					_bIsTestSucces = false;
				}
				else
				{
					return false;
				}
			}

		}
	}

	return true;
}

bool Buff::CheckOneFilter(int _Filter, int _TestProperty, int _Operator)
{

	if (_Operator == 0)
	{
		return _TestProperty >= _Filter;
	}
	else if (_Operator == 1)
	{
		return _TestProperty <= _Filter;
	}
	else if (_Operator == 2)
	{
		return _TestProperty == _Filter;
	}

}

Buff::Buff()
{
	std::string _buf;

	std::cout << "\nEnter the name of the new buff and press Enter:\n";
	std::getline(std::cin, Name);



	std::cout << "\nSelect the type number of the new buff and press Enter:\n";
	std::cout << "\t0 - DamageBuff.\n";
	std::cout << "\t1 - ProtectionBuff.\n";
	std::cout << "\t2 - SpeedBuff.\n";
	bool _bIsCorrect = false;
	while (!_bIsCorrect)
	{
		std::getline(std::cin, _buf);
		if (_buf[0] >= 48 && _buf[0] < 48 + N_OF_BUFF_TYPES)
		{
			Type = static_cast<EBuffType>(std::stoi(_buf.assign(_buf, 0, 1)));
			_bIsCorrect = true;
		}
		else
		{
			std::cout << "Invalid number entered.\n";
		}
	}


	std::cout << "\nEnter the buff value and press Enter:\n";
	_bIsCorrect = false;
	while (!_bIsCorrect)
	{
		std::getline(std::cin, _buf);
		if (_buf[0] > 47 && _buf[0] < 58)
		{
			Value = std::stod(_buf);
			_bIsCorrect = true;
		}
		else
		{
			std::cout << "Invalid value entered.\n";
		}
	}

	AddFilters();
}


void Buff::AddFilters()
{
	std::string _buf;
	Filters.assign(3, -1);

	int _ItTypeMark = 0;
	int _ItLevelMark = 1;

	bool _AllFiltersAdd = false;
	bool _bIsCorrect = false;

	while (!_AllFiltersAdd)
	{
		std::cout << "\nSelect the type number of the filter to be added and press Enter:\n";
		std::cout << "\t1 - By item level.\n";
		std::cout << "\t2 - By item type.\n";
		std::cout << "\t3 - Item rarity.\n";
		std::cout << "\t4 - Finish adding filters\n";
		std::getline(std::cin, _buf);
		if (_buf[0] == 49)
		{
			std::cout << "\nSelect the sign number and press Enter:\n";
			std::cout << "\t0 - >=.\n";
			std::cout << "\t1 - <=.\n";
			std::cout << "\t2 - ==.\n";
			_bIsCorrect = false;
			while(!_bIsCorrect)
			{
				std::getline(std::cin, _buf);
				if (_buf[0] >= 48 && _buf[0] <=50)
				{
					Filters.emplace(Filters.begin() + _ItLevelMark, std::stoi(_buf.assign(_buf, 0, 1)));
					++_ItLevelMark;
					_bIsCorrect = true;
				}
				else
				{
					std::cout << "Invalid number entered.\n";
				}
			}

			std::cout << "\nEnter the level at which the filter will work and press Enter:\n";
			_bIsCorrect = false;
			while (!_bIsCorrect)
			{
				std::getline(std::cin, _buf);
				if (_buf[0] > 47 && _buf[0] < 58)
				{
					Filters.emplace(Filters.begin() + _ItLevelMark, std::stoi(_buf));
					++_ItLevelMark;
					_bIsCorrect = true;
				}
				else
				{
					std::cout << "Invalid value entered.\n";
				}
			}

		}
		else if (_buf[0] == 50)
		{
			std::cout << "Select the item type number for which the filter will work and press Enter:\n";
			std::cout << "\t0 - Melee.\n";
			std::cout << "\t1 - Range.\n";
			std::cout << "\t2 - Armour.\n";
			_bIsCorrect = false;
			while (!_bIsCorrect)
			{
				std::getline(std::cin, _buf);
				if (_buf[0] >= 48 && _buf[0] < 48 + N_OF_ITEM_TYPES)
				{
					Filters.emplace(Filters.begin() + _ItTypeMark, std::stoi(_buf.assign(_buf, 0, 1)));
					++_ItTypeMark;
					++_ItLevelMark;
					_bIsCorrect = true;
				}
				else
				{
					std::cout << "Invalid number entered.\n";
				}
			}
		}
		else if (_buf[0] == 51)
		{
			std::cout << "\nSelect the rarity number of the item for which the filter will work, and press Enter:\n";
			std::cout << "\t0 - Common.\n";
			std::cout << "\t1 - Rare.\n";
			std::cout << "\t2 - Epic.\n";
			_bIsCorrect = false;
			while (!_bIsCorrect)
			{
				std::getline(std::cin, _buf);
				if (_buf[0] >= 48 && _buf[0] < 48 + N_OF_ITEM_RARITY)
				{
					Filters.emplace(Filters.begin() + _ItLevelMark + 1, std::stoi(_buf.assign(_buf, 0, 1)));
					_bIsCorrect = true;
				}
				else
				{
					std::cout << "Invalid number entered.\n";
				}
			}

		}
		else if (_buf[0] == 52)
		{
			_AllFiltersAdd = true;
			std::cout << std::endl;
		}
	}
}
