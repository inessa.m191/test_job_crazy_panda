#pragma once
#include "Item.h"


class Weapon : public Item
{
	double BaseDamage;
	double BuffDamage;
	double BaseSpeed;
	double BuffSpeed;

public:
	Weapon(EItemType _Type);

	void DisplayItem() override;

	void SelfBuff(EBuffType _BuffType, double _Value) override;
};

