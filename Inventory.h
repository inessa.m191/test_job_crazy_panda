#pragma once
#include <vector>
#include "Types.h"

class Item;

class Inventory
{
	std::vector<Item*> AllItems;
	int NOfItems = 0;

public:


	void AddItemToInventory();

	void DisplayInventory();

	int GetNOfItems()
	{
		return NOfItems;
	}

	Item* GetItemByN(int _N)
	{
		return AllItems[_N];
	}

	~Inventory()
	{
		for (auto _item : AllItems)
		{
			delete _item;
		}
	}
};

